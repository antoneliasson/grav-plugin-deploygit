<?php
namespace Grav\Plugin;

require_once('Git.php');

use Grav\Common\Page\Page;
use Grav\Common\Plugin;
use Grav\Common\Uri;
use Grav\Common\Taxonomy;

class DeployGitPlugin extends Plugin
{
    public static function getSubscribedEvents() {
        return [
                'onPluginsInitialized' => ['onPluginsInitialized', 0],
                ];
    }

    public function onPluginsInitialized()
    {
        $enabled = $this->config->get('plugins.deploygit.enabled');
        if ($enabled) {
            $uri = $this->grav['uri'];
            $route = $this->config->get('plugins.deploygit.route');

            if ($route && $route == $uri->path()) {
                $repo = $this->config->get('plugins.deploygit.repository');
                if ($repo === null) {
                    throw new \Exception('plugins.deploygit.repository not set');
                }
                $git = \Git::open($repo);
                try {
                    $git->pull('', '');
                } catch (\Exception $e) {
                    http_response_code(500);
                    echo("Pull failed:\n".$e->getMessage()."\n");
                    exit(1);
                }
                echo 'Success';
                exit();
            }
        }
    }
}
?>
