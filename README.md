# Grav deploy-from-git plugin

This plugin allows you to easily deploy your site from a Git repository. Simply install this plugin and make sure that your site's local Git repository has an upstream remote configured. Then set up a webhook with your favorite Git hosting service to call this plugin on every push, to automatically pull down and deploy your website.

## Credits

This plugin uses a bundled version of [Git.php by kbjr](https://github.com/kbjr/Git.php) to operate on the Git repository.

# GPM Installation

You probably know the drill already:

    $ bin/gpm install deploygit

If not, see the [documentation for GPM](http://learn.getgrav.org/advanced/grav-gpm).

# Setup

## Repository settings

Your local repository must have a remote configured as upstream. I recommend using a separate *deployment key* on the web server for this purpose. If you can run `git pull` without arguments then you're all set. Otherwise you can set an upstream like so:

     $ git branch --set-upstream-to=origin/master master

## deploygit configuration

deploygit has the following parameters:

 - enabled
 - route
 - repository

`route` is the URL path that runs this plugin. `repository` is an **absolute** path to the root of the Git repository on your web server.

See deploygit.yaml for default values. Create a */user/config/plugins/deploygit.yaml* file if you need to override any of them.

No client submitted parameters are taken from requests to the deploy URL so the URL is not sensitive. The idea is that the upstream repository always contains the latest version that should be deployed, so if the deploy URL is hit multiple times then git will do nothing. If you still think it's scary that anyone on the Internet can run `git pull` on your webserver then feel free to set `route` to a secret string. It is not user visible anywhere on the site.

## Webhook configuration

Configure your hosting provider to make a HTTP request (any method) to the deploy URL on every push. The plugin responds with HTTP 200 on success or HTTP 500 on failure.

Example using Bitbucket:

![](example-bitbucket.png)

Github and Gitlab also offers webhooks that should be similarly easy to set up.
