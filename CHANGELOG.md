# v0.2.0
## 2016-08-28

1. [](#new)
    * Implement the `enabled` option.

# v0.1.0
## 2016-06-12

1. [](#new)
    * First public release.
